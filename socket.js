
module.exports = io => {
    const { Notificaciones } = require('./notificacion')
    const notificacion = new Notificaciones();
    const Sos = require('./sosmodel')
    let usernames = {}
    var rooms = []
    
    io.on('connection', async client => {
        console.log('master en linea')
        client.on('EntrarChatPrivado',async (data)=>{
            console.log(data)
            client.room = data.empresa
            usernames[data.empresa] = {id:client.id}
            client.join(client.room)
            console.log(usernames)
            viejos = await Sos.find(
                {empresa:data.empresa}
            )
            io.in(data.empresa).emit('recibiralerta', viejos)
            console.log(viejos)
        })
        client.on('EnviarMensajePrivado',async (data)=>{
            const nombre = data.name;
            const empresa = data.empresa;
            console.log(data)
            const sos = new Sos({
            name: data.name,
            zone: data.zone,
            user: data.user,
            empresa: data.empresa,
            company: data.company,
            img: data.img,
            audio: data.audio,
            message: data.message,
            })
            await sos.save()
            viejos = await Sos.find({
                company: data.empresa
            }).limit(1).sort({$natural: -1})
            io.in(data.empresa).emit('recibiralerta', viejos )
            const aler = "S.O.S. de "+data.name
            const msg = {
                app_id:"00750a14-5dd4-4499-8871-9e86cf68ba5a",
                contents:{"en":aler,"es":aler},
                small_icon:"ic_stat_onesignal_default",
                android_accent_color:"3a74a5 ",
                filters:[{"field" : "tag","key":"id","relation":"=","value":"2"},{"operator":"AND"},
                        {"field" : "tag","key":"idE","relation":"=","value":data.empresa}
                    ]  
            }
            notificacion.sendNotificacion(msg)
        })
        // client.on('disconnect',async (data)=>{
        //     usernames = usernames.filter(empresa => empresa !== data.empresa)
        //     console.log(usernames)
        // })
    })
}