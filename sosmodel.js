var mongoose = require('mongoose');
const { Schema } = mongoose;
var sosmodel = new Schema({
    name: String,
    zone: String,
    user: String,
    empresa: String,
    company: String,
    img: String,
    audio: String,
    message: String,
    date: {
        type: Date, default: Date.now()
    }
})
module.exports = mongoose.model('sos',sosmodel)